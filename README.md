**Projeto Site Acerta**

---

## Resumo

Desenvolvimento de um site, usando React.js

--

## Autor

1. Douglas da Silva Lunardi

---

## Uso

Para este projeto foi utilizado o ecossitema listado abaixo:
- React Router;
- Formik;
- Yup;
- React-icons;
- React-input-Mask;
- Axios;

---

### `npm start`

Executa o aplicativo no modo de desenvolvimento. <br />
Abra [http://localhost:3000](http://localhost:3000)  para visualizá-lo no navegador.


### `json-server`

Para manuseio do acertaleads.json <br />
Execute json-server acertaleads.json --watch -p 3333
