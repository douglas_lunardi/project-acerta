import axios from "axios";

const api = axios.create({
  baseURL: "",
});

api.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (error.response.status === 401) {
  return Promise.reject(error);
}
})

export default api;