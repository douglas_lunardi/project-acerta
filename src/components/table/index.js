import React from 'react';
import { BsFillTrashFill, BsPencilSquare } from "react-icons/bs";
import { Link } from 'react-router-dom';
import axios from 'axios';

export default class Table extends React.Component {
    // constructor(props) {
    //     super(props)
    // }

    deleteLead = (lead) => {
        axios.delete(`http://localhost:3333/leads/${lead.id}`)
            .then(res => {
                this.props.callback();
            })
    }

    listingItens() {
        let dados = [];

        this.props.dataSource.forEach((item) => {
            dados.push(
                <tr key={item}>
                    <th>
                        <Link to={`/register/${item.id}`}>
                            <BsPencilSquare value={{ className: "icons" }} />
                        </Link>

                        <BsFillTrashFill value={{ className: "icons" }} onClick={() => { this.deleteLead(item) }} />
                    </th>
                    <td>{item.email}</td>
                    <td>{item.nome}</td>
                    <td>{item.cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4")}</td>
                </tr>
            );
        });
        return dados;
    }

    render() {
        return (
            <table className="table" responsive>
                <thead>
                    <tr>
                        <th className="col-1"></th>
                        <th>Email</th>
                        <th>Nome</th>
                        <th>CPF</th>
                    </tr>
                </thead>
                <tbody>
                    {this.listingItens()}
                </tbody>
            </table>
        );
    }
}