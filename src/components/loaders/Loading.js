import React from "react";
import { render } from "react-dom";

export default class Loader extends React.Component{
    render(){        
      let { props } = this;
      if (props.error) {
        return (
          <div>
            Ops! Ocorreu um erro{" "}
            <button className="btn btn-default" onClick={props.retry}>
              Tente novamente
            </button>
          </div>
        );
      } else if (props.pastDelay) {
        return (
          <div className="loader">
            <div className="lds-dual-ring"></div>
          </div>
        );
      } else {
        return null;
      }
    }
}