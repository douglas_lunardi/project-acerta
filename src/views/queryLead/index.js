// import "../../style/style.css";
import React from "react";
import { Link } from "react-router-dom";
import InputMask from "react-input-mask";
import Table from "../../components/table/index";
import axios from "axios";

export default class Listing extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nome: "",
      cpf: "",
      leads: [],
    };
    this.searchLeads = this.searchLeads.bind(this);
  }

  searchLeads(nome, cpf) {
    axios
      .get(`http://localhost:3333/leads`, {
        params: {
          nome: !nome ? null : nome,
          cpf: !cpf
            ? null
            : cpf.replaceAll(".", "").replaceAll("-", "").toString(),
        },
      })
      .then((res) => {
        const leads = res.data;
        this.setState({ leads });
      });
  }

  componentDidMount() {
    this.searchLeads();
  }

  render() {
    return (
      <div className="main">
        {/* <div className="container">
          <div className="row" style={{ marginBottom: "30px" }}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="200"
              viewBox="0 0 76.805 14"
            >
              <g transform="translate(-300.369 -212.598)">
                <path
                  class="a"
                  d="M395.867,221.813h-3.151l1.555-3.623Zm1.949,4.745h3.446v-.512L395.04,212.6h-1.516l-6.2,13.449v.512h3.446l.807-1.733h5.435Zm-15.372-10.81v10.8h3.485v-10.8H389.3V212.6H379.057v3.151Zm-9.273,3.4h-2.52v-3.446c.827,0,1.693-.02,2.52,0,2.008.02,1.91,3.446,0,3.446m2.914,2.3c3.485-2.363,2.5-8.839-2.914-8.859h-6.045v13.964h3.524v-4.278h1.831l2.363,4.278h3.938v-.512ZM365.56,212.6h-8.6v13.956h8.782v-3.131h-5.277V221h4.686V217.87h-4.686v-1.949h5.1Zm-23.455,7.073c.02,4.686,3.589,6.911,7.093,6.891,2.934,0,6-1.4,6.513-5.3h-3.466a2.808,2.808,0,0,1-3.047,2.1,3.481,3.481,0,0,1-3.451-3.691c0-2.284,1.359-3.84,3.426-3.84a2.893,2.893,0,0,1,3.013,2.028h3.465c-.531-3.682-3.623-5.237-6.478-5.237a6.816,6.816,0,0,0-7.069,7.049"
                  transform="translate(-28.682 0)"
                />
                <path
                  class="a"
                  d="M308.915,221.814h-3.151l1.556-3.623Zm1.949,4.745h3.446v-.512L308.088,212.6h-1.516l-6.2,13.448v.512h3.446l.807-1.733h5.435Z"
                  transform="translate(0 -0.001)"
                />
                <path
                  class="a"
                  d="M331.287,251.687l.816,1.774h3.715l-.824-1.774Z"
                  transform="translate(-21.248 -26.863)"
                />
                <rect
                  class="b"
                  width="3.157"
                  height="10.66"
                  transform="translate(374.017 212.598)"
                />
                <rect
                  class="b"
                  width="3.157"
                  height="1.738"
                  transform="translate(374.017 224.824)"
                />
              </g>
            </svg>
          </div>
        </div> */}
        <div className="container" style={{ marginBottom: "15px" }}>
          <div className="row">
            <h3 className="titles">Consulta de Leads</h3>
          </div>
        </div>
        <div className="container">
          <div className="row search">
            <div className="col-sm-12 col-md-12 col-lg-12">
              <p>Filtros</p>
            </div>
            <div className="form-group col-sm-12 col-md-6 col-lg-6">
              <label for="name">Nome</label>
              <input
                type="text"
                className="form-control"
                id="name"
                placeholder="Nome"
                onChange={(e) => {
                  this.setState({ nome: e.target.value });
                }}
                required
              />
            </div>
            <div className="form-group col-sm-12 col-md-6 col-lg-6">
              <label for="cpf">CPF</label>
              <InputMask
                mask="999.999.999-99"
                className="form-control"
                id="cpf"
                placeholder="CPF"
                onChange={(e) => {
                  this.setState({ cpf: e.target.value });
                }}
              />
            </div>
            <div className="form-group col-sm-12 col-md-12 col-lg-12">
              <button
                className="btn btn-right"
                onClick={() => {
                  this.searchLeads(this.state.nome, this.state.cpf);
                }}
              >
                Filtrar
              </button>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row">
            <div className="">
              <Link to={`/registerLead/${0}`}>
                <button className="btn"> Novo Lead</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="container" style={{ marginTop: "20px" }}>
          <div className="row">
            <Table
              callback={() => {
                this.searchLeads();
              }}
              dataSource={this.state.leads}
              teste={1}
            >
              {" "}
            </Table>
          </div>
        </div>
      </div>
    );
  }
}
