import '../../style/style.css';
import React from 'react';
import { Link } from 'react-router-dom';
import InputMask from 'react-input-mask';
import { Redirect } from 'react-router';
import axios from 'axios';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

// const regexCPF = /([0-9]{2}[\.]?[0-9]{3}[\.]?[0-9]{3}[\/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[\.]?[0-9]{3}[\.]?[0-9]{3}[-]?[0-9]{2})/gm;
const regexCPF = /([0-9]{2}[.]?[0-9]{3}[.]?[0-9]{3}[/]?[0-9]{4}[-]?[0-9]{2})|([0-9]{3}[.]?[0-9]{3}[.]?[0-9]{3}[-]?[0-9]{2})/gm;

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      estadoCivilBlocked: true,
      initialValues: {
        id: 0,
        nome: '',
        cpf: '',
        email: '',
        estadoCivil: '1', // 1 solteiro
        nomeConjugue: ''
      }
    };

    if (props.match.params.id > 0) {
      axios.get(`http://localhost:3333/leads`, {
        params: {
          id: props.match.params.id
        }
      })
        .then(res => {
          const result = res.data[0];
          const initialValues = {
            id: result.id,
            nome: result.nome,
            email: result.email,
            cpf: result.cpf,
            estadoCivil: result.estadoCivil,
            nomeConjugue: result.nomeConjugue
          }
          this.setState({ initialValues });
        })
    }
  }

  changeEstadoCivil = (event) => {
    let estadoCivilBlocked = true;

    if (event.target.value === "2") {
      estadoCivilBlocked = false;
      this.setState(prevState => ({
        initialValues: {
          ...prevState.initialValues,
          estadoCivil: '2'
        }
      }))
    }
    else {
      this.setState(prevState => ({
        initialValues: {
          ...prevState.initialValues,
          nomeConjugue: '',
          estadoCivil: '999' // mudado o valor do estado civil só para remover a obrigatoriedade do nome do conjugue
        }
      }))
    }

    this.setState({ estadoCivilBlocked });
  }

  isValidCPF = (cpf) => {
    cpf = cpf.replace(/[^\d]+/g, '');
    if (cpf === '') return false;
    // Elimina CPFs invalidos conhecidos	
    if (cpf.length !== 11 ||
      cpf === "00000000000" ||
      cpf === "11111111111" ||
      cpf === "22222222222" ||
      cpf === "33333333333" ||
      cpf === "44444444444" ||
      cpf === "55555555555" ||
      cpf === "66666666666" ||
      cpf === "77777777777" ||
      cpf === "88888888888" ||
      cpf === "99999999999")
      return false;
    // Valida 1o digito	
    let add = 0;
    for (let i = 0; i < 9; i++)
      add += parseInt(cpf.charAt(i)) * (10 - i);
    let rev = 11 - (add % 11);
    if (rev === 10 || rev === 11)
      rev = 0;
    if (rev !== parseInt(cpf.charAt(9)))
      return false;
    // Valida 2o digito	
    add = 0;
    for (let i = 0; i < 10; i++)
      add += parseInt(cpf.charAt(i)) * (11 - i);
    rev = 11 - (add % 11);
    if (rev === 10 || rev === 11)
      rev = 0;
    if (rev !== parseInt(cpf.charAt(10)))
      return false;
    return true;
  }

  validateCPF = (cpf) => {
    if (!this.isValidCPF(cpf)) {
      alert('CPF Inválido!');
      this.setState(prevState => ({
        initialValues: {
          ...prevState.initialValues,
          cpf: ''
        }
      }))
    }
  }

  handleSubmit = values => {
    if (values.id === 0) {
      axios.post(`http://localhost:3333/leads`, {
        nome: values.nome,
        cpf: values.cpf.replaceAll('.', '').replaceAll('-', ''),
        email: values.email,
        estadoCivil: values.estadoCivil,
        nomeConjugue: values.nomeConjugue,
      })
        .then(res => {
          this.setState({ redirect: true });
        })
    }
    else {
      axios.put(`http://localhost:3333/leads/${values.id}`, {
        nome: values.nome,
        cpf: values.cpf.replaceAll('.', '').replaceAll('-', ''),
        email: values.email,
        estadoCivil: values.estadoCivil,
        nomeConjugue: values.nomeConjugue,
      })
        .then(res => {
          this.setState({ redirect: true });
        })
    }
  }

  render() {
    if (this.state.redirect) {
      return <Redirect push to="/queryLead" />;
    }

    return (
      <div className="main">
        {/* <div className="container">
          <div className="row" style={{ marginBottom: '30px' }}>
            <svg xmlns="http://www.w3.org/2000/svg" width="200" viewBox="0 0 76.805 14">
              <g transform="translate(-300.369 -212.598)">
                <path class="a" d="M395.867,221.813h-3.151l1.555-3.623Zm1.949,4.745h3.446v-.512L395.04,212.6h-1.516l-6.2,13.449v.512h3.446l.807-1.733h5.435Zm-15.372-10.81v10.8h3.485v-10.8H389.3V212.6H379.057v3.151Zm-9.273,3.4h-2.52v-3.446c.827,0,1.693-.02,2.52,0,2.008.02,1.91,3.446,0,3.446m2.914,2.3c3.485-2.363,2.5-8.839-2.914-8.859h-6.045v13.964h3.524v-4.278h1.831l2.363,4.278h3.938v-.512ZM365.56,212.6h-8.6v13.956h8.782v-3.131h-5.277V221h4.686V217.87h-4.686v-1.949h5.1Zm-23.455,7.073c.02,4.686,3.589,6.911,7.093,6.891,2.934,0,6-1.4,6.513-5.3h-3.466a2.808,2.808,0,0,1-3.047,2.1,3.481,3.481,0,0,1-3.451-3.691c0-2.284,1.359-3.84,3.426-3.84a2.893,2.893,0,0,1,3.013,2.028h3.465c-.531-3.682-3.623-5.237-6.478-5.237a6.816,6.816,0,0,0-7.069,7.049" transform="translate(-28.682 0)" /><path class="a" d="M308.915,221.814h-3.151l1.556-3.623Zm1.949,4.745h3.446v-.512L308.088,212.6h-1.516l-6.2,13.448v.512h3.446l.807-1.733h5.435Z" transform="translate(0 -0.001)" /><path class="a" d="M331.287,251.687l.816,1.774h3.715l-.824-1.774Z" transform="translate(-21.248 -26.863)" /><rect class="b" width="3.157" height="10.66" transform="translate(374.017 212.598)" /><rect class="b" width="3.157" height="1.738" transform="translate(374.017 224.824)" /></g>
            </svg>
          </div>
        </div> */}
        <div className="container" style={{ marginBottom: '15px' }}>
          <div className="row">
            <h3 className="titles">Cadastro de Leads</h3>
          </div>
        </div>

        <Formik
          enableReinitialize
          initialValues={this.state.initialValues}
          validationSchema={() => Yup.lazy((values) => {
            const isMarried = values.estadoCivil === "2";

            return Yup.object().shape({
              nome: Yup.string()
                .required('Nome é obrigatório'),
              cpf: Yup.string()
                .required('CPF é obrigatório')
                // .transform(function removeNonNumericalChar(value) {
                //   console.log("Transforming", value);
                //   return this.isType(value) && value !== null ? value.replace(/\D/g, "") : value;
                // })
                .matches(regexCPF, "Formato inválido"),
              email: Yup.string()
                .email('Email é inválido')
                .required('Email é obrigatório'),
              estadoCivil: Yup.string()
                .required('Estado Civil é obrigatório'),
              nomeConjugue: isMarried ? Yup.string().required('Nome Cônjugue é obrigatório') : Yup.string()
            })
          })}
          // validationSchema={Yup.object().shape({
          //   nome: Yup.string()
          //     .required('Nome é obrigatório'),
          //   cpf: Yup.string()
          //     .required('CPF é obrigatório')
          //     .min(14),
          //   email: Yup.string()
          //     .email('Email é inválido')
          //     .required('Email é obrigatório'),
          //   estadoCivil: Yup.string()
          //     .required('Estado Civil é obrigatório'),
          //   estadoCivilBlocked: Yup.boolean(),
          //   nomeConjugue: Yup.string()
          //     .when("estadoCivilBlocked", {
          //       is: false,
          //       then: Yup.string().required('Nome Cônjugue é obrigatório')
          //     })
          // })}
          onSubmit={fields => {
            this.handleSubmit(fields)
          }}
          render={({ errors, status, touched, handleChange }) => (
            <Form>
              <div className="container">
                <div className="row search">
                  <div className="col-sm-12 col-md-12 col-lg-12">
                    <p>Filtros</p>
                  </div>
                  <div className="form-group col-sm-12 col-md-6 col-lg-6">
                    <label for="nome">Nome</label>
                    <Field name="nome" type="text" className={'form-control' + (errors.nome && touched.nome ? ' is-invalid' : '')} value={this.state.nome}
                      onChange={(e) => {
                        const nome = e.target.value;
                        this.setState(prevState => ({
                          initialValues: {
                            ...prevState.initialValues,
                            nome: nome
                          }
                        }))
                      }}
                    />
                    <ErrorMessage name="nome" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group col-sm-12 col-md-6 col-lg-6">
                    <label for="cpf">CPF</label>
                    <Field name="cpf" onChange={handleChange}
                      
                      render={({ field }) => {
                        return <InputMask mask="999.999.999-99"
                          {...field}
                          className={'form-control' + (errors.cpf && touched.cpf ? ' is-invalid' : '')}
                          onChange={(e) => {
                            const cpf = e.target.value;
                            this.setState(prevState => ({
                              initialValues: {
                                ...prevState.initialValues,
                                cpf: cpf
                              }
                            }))
                          }}
                          onBlur={e => {
                            this.validateCPF(e.currentTarget.value)
                          }}
                        />
                      }} />

                    <ErrorMessage name="cpf" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group col-sm-12 col-md-6 col-lg-6">
                    <label for="email">Email</label>
                    <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')}
                      onChange={(e) => {
                        const email = e.target.value;
                        this.setState(prevState => ({
                          initialValues: {
                            ...prevState.initialValues,
                            email: email
                          }
                        }))
                      }} />
                    <ErrorMessage name="email" component="div" className="invalid-feedback" />

                  </div>
                  <div className="form-group col-sm-12 col-md-6 col-lg-6">
                    <label for="estadoCivil">Estado Civil</label>
                    <select id="estadoCivil" className={'custom-select form-control' + (errors.estadoCivil && touched.estadoCivil ? ' is-invalid' : '')}
                      onChange={(event) => this.changeEstadoCivil(event)} >
                      <option value="1">Solteiro(a)</option>
                      <option value="2">Casado(a)</option>
                      <option value="3">Víuvo(a)</option>
                      <option value="4">Separado(a)</option>
                    </select>
                    <ErrorMessage name="estadoCivil" component="div" className="invalid-feedback" />
                  </div>
                  <div className="form-group col-sm-12 col-md-6 col-lg-6">
                    <label for="nomeConjugue">Nome do Cônjugue</label>
                    <Field name="nomeConjugue" type="text"
                      className={'form-control'
                        + (errors.nomeConjugue && touched.nomeConjugue && !this.state.estadoCivilBlocked ? ' is-invalid' : '')
                        + (this.state.estadoCivilBlocked ? ' input-blocked' : '')}
                      disabled={this.state.estadoCivilBlocked}
                    />
                    <ErrorMessage name="nomeConjugue" component="div" className="invalid-feedback" />
                  </div>

                  <div className="form-group col-sm-12 col-md-12 col-lg-12">
                    <Link to="/queryLead">
                      <button className="btn btn-cancel"> Cancelar </button>
                    </Link>
                    <button className="btn btn-right" type="submit"> Cadastrar </button>
                  </div>
                </div>
              </div>
            </Form>
          )}
        />
      </div>
    )
  }
}
