import "bootstrap/dist/css/bootstrap.min.css";
import "./style/style.css";
import React from "react";
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
// import loadable from "react-loadable";

import Home from "./views/home/index";
import Listing from "./views/queryLead/index";
import Register from "./views/registerLead/index";
import Sidebar from "./components/sidebar/index";

const App = () => (
  <BrowserRouter>
    <div id="app">
      <Sidebar/>

      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/queryLead" component={Listing} />
        <Route exact path="/registerLead/:id" component={Register} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default App;
